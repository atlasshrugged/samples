# README #

Samples is a number of simple modules, aimed to to show how to use some technologies and products.

## tomcat7embedNoWebXml ##

Example of web servlet containing no web.xml file. It requires Servlet 3.0.
Tests contains a class to start it inside of embedded Tomcat 7.

Todo: SSL

## How to build ##

Use gradle.

### Dependencies ###
Dependencies are described in *build.gradle* file
