package org.ft.samples;

import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

/**
 * @author atlasshrugged
 */
@ApplicationPath("pathApp")
public class TestApp extends ResourceConfig {
    public TestApp() {
        System.out.println("TestApp created");
        packages("org.ft.samples");
//        packages("org.foo.rest;org.bar.rest");
//        register(TimeServlet.class);

    }
}
