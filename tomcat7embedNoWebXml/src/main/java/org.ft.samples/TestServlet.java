package org.ft.samples;

import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.TextNode;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

/**
 * Simple class, that can produce
 *
 * @author atlasshrugged
 */
@Path("pathClass")
public class TestServlet {

    /**
     * Will be executed on .../pathclass request
     *
     * @return text string
     */
    @GET
    public String main() {
        return "main passed";
    }

    /**
     * Will be executed on .../pathclass/test request
     *
     * @return text string
     */
    @GET
    @Path("test")
    public String test() {
        return "test passed";
    }

    /**
     * Will be executed on .../pathclass/json request
     *
     * @return json object
     */
    @GET
    @Path("json")
    @Produces({MediaType.APPLICATION_JSON})
    public Object json() {
        List<Object> ret = new ArrayList<Object>();
        ret.add(new TextNode("test"));
        ret.add(NullNode.getInstance());
        ret.add(IntNode.valueOf(123));
        return ret;
    }

}
