package org.ft.samples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Date;

/**
 * @author atlasshrugged
 */
@ServerEndpoint(value = "/websocket")
public class TestWebSocket {
    private static final Logger log = LoggerFactory.getLogger(TestWebSocket.class);
    private Session session;

    public TestWebSocket() {
        log.info("TestWebSocket created");
    }

    @OnOpen
    public void start(Session session) {
        this.session = session;
        sessionInfo(session);
        respond("start");
    }

    private static void sessionInfo(Session session) {
        System.out.println("starting " + session);
        System.out.println("getNegotiatedSubprotocol: " + session.getNegotiatedSubprotocol());
        System.out.println("getId: " + session.getId());
        System.out.println("getProtocolVersion: " + session.getProtocolVersion());
        System.out.println("getQueryString: " + session.getQueryString());
        System.out.println("getMaxTextMessageBufferSize: " + session.getMaxTextMessageBufferSize());

        System.out.println("  getNegotiatedExtensions size=" + session.getNegotiatedExtensions().size());
        for (Extension i : session.getNegotiatedExtensions()) {
            System.out.print("  getNegotiatedExtensions: " + i.getName() + " - ");
            for (Extension.Parameter p : i.getParameters()) {
                System.out.print(p.getName() + ":" + p.getValue());
            }
            System.out.println();
        }

        System.out.println("getPathParameters: " + session.getPathParameters());
        System.out.println("getRequestParameterMap: " + session.getRequestParameterMap());
        System.out.println("getRequestURI: " + session.getRequestURI());
        System.out.println("isOpen: " + session.isOpen());
        System.out.println("isSecure: " + session.isSecure());
    }

    private void respond(String message) {
        try {
            log.info("respond: {}", message);
            session.getBasicRemote().sendText(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @OnClose
    public void end(CloseReason reason) {
        System.out.println(" - " + reason);
        System.out.println("ending " + session);
//		respond("end");
        this.session = null;
    }

    @OnMessage
    public void incoming(String message) {
        System.out.println("incoming '" + message + "' for " + session);
        respond(new Date() + ": " + message);
    }

}
