package org.ft.samples.lowlevel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.util.Date;

/**
 * Old-style filter
 *
 * @author atlasshrugged
 */
@WebFilter("/filter")
public class LowFilter implements Filter {
    Logger log = LoggerFactory.getLogger(LowFilter.class);

    {
        System.out.println("LowFilter created");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        response.setContentType("text/plain");
        response.getWriter().write(new Date().toString());

//        chain.doFilter(request, response);
//        if (response instanceof HttpServletResponse) {
//            log.info("Applying cache control filter to response");
//            HttpServletResponse httpServletResponse = (HttpServletResponse)response;
//            httpServletResponse.setHeader("Cache-Control", "nocache");
//        }
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
        log.debug("init: {}", config.getServletContext().getContextPath());
    }

    @Override
    public void destroy() {
    }
}
