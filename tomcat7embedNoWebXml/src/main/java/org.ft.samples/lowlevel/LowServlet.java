package org.ft.samples.lowlevel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

/**
 * Old-style servlet
 *
 * @author atlasshrugged
 */
@WebServlet(value = "/sample", loadOnStartup = 1)
public class LowServlet extends HttpServlet {
    Logger log = LoggerFactory.getLogger(LowFilter.class);

    {
        System.out.println("LowServlet created");
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        log.debug("init: {}", config.getServletContext().getContextPath());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/plain");
        response.getWriter().write(new Date().toString());
    }
}
