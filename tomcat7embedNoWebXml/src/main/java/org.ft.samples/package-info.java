/**
 * port - TomcatServerMain.port
 * pathServlet - servlet name or TomcatServerMain.appBase
 * pathApp - servlet name or TomcatServerMain.appBase
 * pathClass - servlet name or TomcatServerMain.appBase
 *
 * index.html:  http://0.0.0.0:port/pathServlet/
 * HttpServlet: http://0.0.0.0:port/pathServlet/sample
 * Filter:      http://0.0.0.0:port/pathServlet/sample
 *
 * Servlet:     http://0.0.0.0:port/pathServlet/pathApp/pathClass
 * String test: http://0.0.0.0:port/pathServlet/pathApp/pathClass/test
 * Json test:   http://0.0.0.0:port/pathServlet/pathApp/pathClass/json
 *
 * Websocket:   ws://0.0.0.0:port/pathServlet/websocket
 *
 * @author atlasshrugged
 */
package org.ft.samples;