package org.ft.samples.test;

import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.naming.resources.VirtualDirContext;

import java.io.File;

/**
 * See: https://stackoverflow.com/questions/11669507/embedded-tomcat-7-servlet-3-0-annotations-not-working
 *
 * @author atlasshrugged
 */
public class TomcatServerMain {
    private static final String appBase = "/pathServlet";
    private static final int port = 8088;
    private static final String MODULE = "tomcat7embedNoWebXml"; // '.' for no module
    private static final String WEBAPP = "/src/main/webapp";
    private static final String CLASSES = "/build/classes/main";

    public static void main(String[] args) throws Exception {
        BasicConfigurator.configure();
        Logger.getRootLogger().setLevel(Level.INFO);

        Tomcat tomcat = new Tomcat();
        tomcat.setPort(port);

        File f = new File(MODULE + WEBAPP);
        System.err.println(fileInfo(f));
        StandardContext ctx = (StandardContext) tomcat.addWebapp(appBase, f.getAbsolutePath());

        //declare an alternate location for your "WEB-INF/classes" dir:
        File additionWebInfClasses = new File(MODULE + CLASSES);
        System.err.println(fileInfo(f));
        VirtualDirContext resources = new VirtualDirContext();
        resources.setExtraResourcePaths("/WEB-INF/classes=" + additionWebInfClasses);
        /*
         * In Tomcat 8:
         * WebResourceRoot resources = new StandardRoot(ctx);
         * resources.addPreResources(new DirResourceSet(resources, "/WEB-INF/classes", additionWebInfClasses.getAbsolutePath(), "/"));
         */
        ctx.setResources(resources);

        tomcat.start();
        tomcat.getServer().await();
    }

    private static String fileInfo(File f) {
        return "resource: '" + f.getAbsolutePath() + (f.exists() ? "' exists" : "' not found");
    }
}
